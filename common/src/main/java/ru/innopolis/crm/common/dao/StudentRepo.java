package ru.innopolis.crm.common.dao;

import org.springframework.stereotype.Repository;

/**
 * Created by Artem Panasyuk on 16.05.2017.
 */

@Repository
public interface StudentRepo <Student,Long>{
}
