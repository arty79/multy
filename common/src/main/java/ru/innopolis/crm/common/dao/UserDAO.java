package ru.innopolis.crm.common.dao;


import ru.innopolis.crm.common.entity.User;

/**
 *
 */
public interface UserDAO extends DAO<Long, User> {

    User findUserByLoginAndPassword(String login, String password);
}
