package ru.innopolis.crm.common.dao;


import ru.innopolis.crm.common.entity.Student;

/**
 *
 */
public interface StudentDAO extends DAO<Long, Student> {


    String getStudentLogin(Student student);
}
