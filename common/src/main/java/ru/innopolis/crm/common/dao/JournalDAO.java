package ru.innopolis.crm.common.dao;


import ru.innopolis.crm.common.entity.Journal;

/**
 *
 */
public interface JournalDAO extends DAO<Long, Journal> {
}
