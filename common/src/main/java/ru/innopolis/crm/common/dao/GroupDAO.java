package ru.innopolis.crm.common.dao;


import ru.innopolis.crm.common.entity.Group;

/**
 *
 */
public interface GroupDAO extends DAO<Long, Group> {


}
