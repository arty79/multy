package ru.innopolis.crm.common.dao;


import ru.innopolis.crm.common.entity.Lesson;

/**
 *
 */
public interface LessonDAO extends DAO<Long, Lesson> {
}
